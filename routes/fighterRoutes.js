const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    let fighters = FighterService.getAllFighters();

    if (fighters) {
      res.data = fighters;
    } else {
      res.err = {
        error: true,
        message: "Fighters not found",
      };
      res.status(404);
    }
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    const fighter = FighterService.getFighter(req.params.id);

    if (fighter) {
      res.data = fighter;
    } else {
      res.err = {
        error: true,
        message: "Fighter not found",
      };
      res.status(404);
    }

    next();
  },
  responseMiddleware
);

router.post(
  "/",
  createFighterValid,
  (req, res, next) => {
    const fighter = FighterService.createFighter(req.body);

    if (fighter) {
      res.data = fighter;
    } else {
      res.err = {
        error: true,
        message: "Fighter is not created",
      };
    }

    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  (req, res, next) => {
    const fighter = FighterService.updateFighter(req.params.id, req.body);

    if (fighter) {
      res.data = fighter;
    } else {
      res.err = {
        error: true,
        message: "Fighter is not updated",
      };
    }

    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    const fighter = FighterService.deleteFighter(req.params.id);

    if (fighter) {
      res.data = fighter;
      res.status(204);
    } else {
      res.err = {
        error: true,
        message: "Fighter is not deleted",
      };
    }

    next();
  },
  responseMiddleware
);

module.exports = router;
