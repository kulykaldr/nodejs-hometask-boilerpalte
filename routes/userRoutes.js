const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    let users = UserService.getAllUsers();

    if (users) {
      res.data = users;
    } else {
      res.err = {
        error: true,
        message: "Users not found",
      };
      res.status(404);
    }
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    const user = UserService.getUser(req.params.id);

    if (user) {
      res.data = user;
    } else {
      res.err = {
        error: true,
        message: "User not found",
      };
      res.status(404);
    }

    next();
  },
  responseMiddleware
);

router.post(
  "/",
  createUserValid,
  (req, res, next) => {
    const { email, phoneNumber } = req.body;
    const emailSearch = UserService.search({ email });
    if (emailSearch) {
      res.err = {
        error: true,
        message: `User with email ${email} is already exist`,
      };
      return next();
    }

    const phoneSearch = UserService.search({ phoneNumber });
    if (phoneSearch) {
      res.err = {
        error: true,
        message: `User with phone number ${phoneNumber} is already exist`,
      };
      return next();
    }

    const user = UserService.createUser(req.body);

    if (user) {
      res.data = user;
    } else {
      res.err = {
        error: true,
        message: "User is not created",
      };
    }

    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  (req, res, next) => {
    const { email, phoneNumber } = req.body;
    const { id } = req.params;

    const emailSearch = UserService.search({ email });
    if (emailSearch && emailSearch.id !== id) {
      res.err = {
        error: true,
        message: `User with email ${email} is already exist`,
      };
      return next();
    }

    const phoneSearch = UserService.search({ phoneNumber });
    if (phoneSearch && phoneSearch.id !== id) {
      res.err = {
        error: true,
        message: `User with phone number ${phoneNumber} is already exist`,
      };
      return next();
    }

    const user = UserService.updateUser(id, req.body);

    if (user) {
      res.data = user;
    } else {
      res.err = {
        error: true,
        message: "User is not updated",
      };
    }

    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    const user = UserService.deleteUser(req.params.id);

    if (user) {
      res.data = user;
      res.status(204);
    } else {
      res.err = {
        error: true,
        message: "User is not deleted",
      };
      res.status(404);
    }

    next();
  },
  responseMiddleware
);

module.exports = router;
