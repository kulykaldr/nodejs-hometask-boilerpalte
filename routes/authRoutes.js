const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.post(
  "/login",
  (req, res, next) => {
    try {
      // TODO: Implement login action
      const { email, password } = req.body;
      const user = AuthService.login({ email });

      if (user.password !== password) {
        throw Error("Incorrect password");
      }

      res.data = user;
    } catch (err) {
      res.err = {
        error: true,
        message: err.message,
      };
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
