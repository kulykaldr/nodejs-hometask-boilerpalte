const { user } = require("../models/user");

const isAlpha = (str) => {
  return /^[a-zA-Z()]+$/.test(str);
};

const createUserValid = (req, res, next) => {
  if (req.body) {
    // Validation required user fields
    for (let key of Object.keys(user)) {
      if (key === "id") continue;

      if (!req.body[key] || !Object.keys(req.body).includes(key)) {
        return res.status(400).send({
          error: true,
          message: `The ${key} is required user field`,
        });
      }
    }

    // Validation not valid fields
    for (let key of Object.keys(req.body)) {
      if (key === "id" || !Object.keys(user).includes(key)) {
        return res.status(400).send({
          error: true,
          message: `The ${key} is not valid user field`,
        });
      }
    }

    const { firstName, lastName, email, phoneNumber, password } = req.body;

    if (!isAlpha(firstName) || !isAlpha(lastName)) {
      return res.status(400).send({
        error: true,
        message: `First Name and Last Name must contains only english letters`,
      });
    }

    if (!email.includes("@gmail.com")) {
      return res.status(400).send({
        error: true,
        message: `Email must be only from @gmail.com`,
      });
    }

    if (!phoneNumber.startsWith("+380") || phoneNumber.length !== 13) {
      return res.status(400).send({
        error: true,
        message: `Phone number must be +380xxxxxxxxx`,
      });
    }

    if (password.length < 3) {
      return res.status(400).send({
        error: true,
        message: `Password must be at least 3 symbols`,
      });
    }

    next();
  } else {
    res.status(400).json({
      error: true,
      message: "Requested data is not valid",
    });
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  if (!req.params.id) {
    return res.status(400).json({
      error: true,
      message: "No user ID is requested",
    });
  }

  if (req.body) {
    // Validation required user fields
    for (let key of Object.keys(user)) {
      if (key === "id") continue;

      if (!req.body[key] || !Object.keys(req.body).includes(key)) {
        return res.status(400).send({
          error: true,
          message: `The ${key} is required user field`,
        });
      }
    }

    // Validation not valid fields
    for (let key of Object.keys(req.body)) {
      if (key === "id" || !Object.keys(user).includes(key)) {
        return res.status(400).send({
          error: true,
          message: `The ${key} is not valid user field`,
        });
      }
    }

    const { firstName, lastName, email, phoneNumber, password } = req.body;

    if (!isAlpha(firstName) || !isAlpha(lastName)) {
      return res.status(400).send({
        error: true,
        message: `First Name and Last Name must contains only english letters`,
      });
    }

    if (!email.includes("@gmail.com")) {
      return res.status(400).send({
        error: true,
        message: `Email must be only from @gmail.com`,
      });
    }

    if (!phoneNumber.startsWith("+380") || phoneNumber.length !== 13) {
      return res.status(400).send({
        error: true,
        message: `Phone number must be +380xxxxxxxxx`,
      });
    }

    if (password.length < 3) {
      return res.status(400).send({
        error: true,
        message: `Password must be at least 3 symbols`,
      });
    }

    next();
  } else {
    return res.status(400).json({
      error: true,
      message: "Requested data is not valid",
    });
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
