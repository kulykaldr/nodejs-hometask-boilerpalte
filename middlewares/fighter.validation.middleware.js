const { fighter } = require("../models/fighter");

const isAlpha = (str) => {
  return /^[a-zA-Z()]+$/.test(str);
};

const createFighterValid = (req, res, next) => {
  if (req.body) {
    // Validation required fighter fields
    for (let key of Object.keys(fighter)) {
      if (key === "id") continue;

      if (!req.body[key] || !Object.keys(req.body).includes(key)) {
        return res.status(400).send({
          error: true,
          message: `The ${key} is required fighter field`,
        });
      }
    }

    // Validation not valid fields
    for (let key of Object.keys(req.body)) {
      if (!Object.keys(fighter).includes(key)) {
        return res.status(400).send({
          error: true,
          message: `The ${key} is not valid fighter field`,
        });
      }
    }

    const { name, health, power, defense } = req.body;

    if (!isAlpha(name)) {
      return res.status(400).send({
        error: true,
        message: `Name must contains only english letters`,
      });
    }

    if (health < 1 || health > 100) {
      return res.status(400).send({
        error: true,
        message: `Health must be number from 1 to 100`,
      });
    } else {
      req.body.health = Number(health);
    }

    if (power < 1 || power > 10 || defense < 1 || defense > 10) {
      return res.status(400).send({
        error: true,
        message: `Power and Defense must be number from 1 to 10`,
      });
    }

    next();
  } else {
    res.status(400).json({
      error: true,
      message: "Requested data is not valid",
    });
  }
};

const updateFighterValid = (req, res, next) => {
  if (req.body) {
    // Validation required fighter fields
    for (let key of Object.keys(fighter)) {
      if (key === "id") continue;

      if (!req.body[key] || !Object.keys(req.body).includes(key)) {
        return res.status(400).send({
          error: true,
          message: `The ${key} is required fighter field`,
        });
      }
    }

    // Validation not valid fields
    for (let key of Object.keys(req.body)) {
      if (!Object.keys(fighter).includes(key)) {
        return res.status(400).send({
          error: true,
          message: `The ${key} is not valid fighter field`,
        });
      }
    }

    const { name, health, power, defense } = req.body;

    if (!isAlpha(name)) {
      return res.status(400).send({
        error: true,
        message: `Name must contains only english letters`,
      });
    }

    if (health < 1 || health > 100) {
      return res.status(400).send({
        error: true,
        message: `Health must be from 1 to 100`,
      });
    }

    if (power < 1 || power > 10 || defense < 1 || defense > 10) {
      return res.status(400).send({
        error: true,
        message: `Power and Defense must be from 1 to 10`,
      });
    }

    next();
  } else {
    res.status(400).json({
      error: true,
      message: "Requested data is not valid",
    });
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
