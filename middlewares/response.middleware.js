const responseMiddleware = (req, res, next) => {
  if (res && res.err) {
    if (res.statusCode < 400) {
      res.status(400);
    }
    res.json(res.err);
  }

  if (res && res.data) {
    res.json(res.data);
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
