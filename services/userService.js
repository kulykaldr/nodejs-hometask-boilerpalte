const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  getAllUsers() {
    const users = UserRepository.getAll();
    if (!users) {
      return null;
    }
    return users;
  }

  getUser(id) {
    const user = UserRepository.getOne({ id });
    if (!user) {
      return null;
    }
    return user;
  }

  createUser(data) {
    const user = UserRepository.create(data);
    if (!user) {
      return null;
    }
    return user;
  }

  updateUser(id, data) {
    const user = UserRepository.update(id, data);
    if (!user) {
      return null;
    }
    return user;
  }

  deleteUser(id) {
    const user = UserRepository.delete(id);
    if (!user) {
      return null;
    }
    return user;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
