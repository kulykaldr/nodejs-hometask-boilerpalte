const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  getAllFighters() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      return null;
    }
    return fighters;
  }

  getFighter(id) {
    const fighter = FighterRepository.getOne({ id });
    if (!fighter) {
      return null;
    }
    return fighter;
  }

  createFighter(data) {
    const fighter = FighterRepository.create(data);
    if (!fighter) {
      return null;
    }
    return fighter;
  }

  updateFighter(id, data) {
    const fighter = FighterRepository.update(id, data);
    if (!fighter) {
      return null;
    }
    return fighter;
  }

  deleteFighter(id) {
    const fighter = FighterRepository.delete(id);
    if (!fighter) {
      return null;
    }
    return fighter;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
